FROM debian

RUN apt update && apt upgrade -yy && apt install wget git -yy
RUN wget https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz &&\
	tar -xf go1.13.3.linux-amd64.tar.gz && \
	mv go /usr/local/ && \
	ln -s /usr/local/go/bin/go /usr/local/bin/go && \
	GOROOT=/usr/local/go && \
	mkdir -p /tmp/gotty && \
	GOPATH=/tmp/gotty go get github.com/yudai/gotty && \
	mv /tmp/gotty/bin/gotty /usr/local/bin/ && \
	apt remove git -yy && \
	rm -Rf go1.13.3.linux-amd64.tar.gz go && \
	rm -rf /tmp/gotty/ /var/cache/apt/*
RUN apt install nano

WORKDIR /h4cker

ENTRYPOINT ["/usr/local/bin/gotty"]
CMD ["--permit-write", "--reconnect", "/bin/bash"]

